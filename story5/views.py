from django.shortcuts import render
from django.shortcuts import redirect
from .forms import ScheduleForm
from .models import Schedule
import datetime

# Create your views here.
def index(request):
    return render(request, 'index.html')

def aboutme(request):
    return render(request, 'about-me.html')

def resume(request):
    return render(request, 'resume.html')

def gallery(request):
    return render(request, 'gallery.html')

def contact(request):
    return render(request, 'contact.html')

def newevent(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
                post = form.save(commit=False)
                post.save()
                return redirect('story5:listschedule')
    else:
        form = ScheduleForm()

    content = {'title' : 'Form Schedule',
                'form' : form}

    return render(request, 'schedule_event.html', content)

def listschedule(request):
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return render(request, 'schedule.html', content)


def delete(request, pk):
    Schedule.objects.filter(pk=pk).delete()
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return redirect('story5:listschedule')

def deleteall(request):
    if request.method == "POST":
        Schedule.objects.all().delete()
        data = Schedule.objects.all()
        content = {'title' : 'Schedule',
                'data' : data}
        return redirect('story5:listschedule')
    else:
        return redirect('story5:listschedule')


def edit(request, pk):
    post = Schedule.objects.get(pk=pk)
    if request.method == "POST":
        form = ScheduleForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('story5:listschedule')
    else:
        form = ScheduleForm(instance=post)

    content = {'title' : 'Form Schedule',
                'form' : form,
                'list' : post}
    return render(request, 'edit_schedule.html', content)



