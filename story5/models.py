from django.db import models
from django.utils import timezone
import datetime

CATEGORY=[('Personal', 'Personal'),
            ('Work', 'Work'),
            ('Quiz', 'Quiz'),
            ('Exam', 'Exam'),
            ('Homework', 'Homework'),
            ('Lab', 'Lab'),
            ('Other', 'Other')]

class Schedule(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=50, choices=CATEGORY)
