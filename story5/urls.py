from . import views
from django.shortcuts import render
from django.urls import path

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('resume/', views.resume, name='resume'),
    path('gallery/', views.gallery, name='gallery'),
    path('newevent/', views.newevent, name='newevent'),
    path('schedule/', views.listschedule, name='listschedule'),
    path('editschedule/<int:pk>', views.edit, name='edit'),
    path('deleteschedule/<int:pk>', views.delete, name='delete'),
    path('deleteall/', views.deleteall, name='deleteall'),
    path('contact/', views.contact, name='contact'),
]
